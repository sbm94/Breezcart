//
//  ProfileViewController.swift
//  BreezCart
//
//  Created by Sishir Mohan on 2/17/17.
//  Copyright © 2017 Sishir Mohan. All rights reserved.
//


import UIKit
import FirebaseAuth
import FirebaseDatabase
import Stripe

class ProfileViewController: UIViewController {

    //MARK: Properties
    
    
     
    @IBOutlet weak var ProfileNavigationBar: UINavigationBar!
    
    @IBOutlet weak var profileImage: UIImageView!
    
    
    @IBOutlet weak var fullName: UILabel!
  
   
    @IBOutlet weak var viewReceiptsButton: UIButton!
    
    
    @IBOutlet weak var viewManagePaymentButton: UIButton!
    
    
    @IBOutlet weak var LogOut: UIButton!
 
    var disableInteractivePlayerTransitioning = false
    
    var managePaymentViewController: STPPaymentContextViewController!
    var viewReceiptsController: ViewReceiptsController!

    
    
    //@IBOutlet weak var ProfileNavigationBar: UINavigationBar!
    
   // @IBOutlet weak var viewReceiptsButton: UIButton!

/// @IBOutlet weak var viewManagePaymentButton: UIButton!
    
    
   // @IBOutlet weak var LogOut: UIButton!
    
    var ref: FIRDatabaseReference?
    var uid = FIRAuth.auth()?.currentUser?.uid
    

    @IBAction func ViewReceipts(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name:"Main", bundle:nil)
        let viewReceipts: UITableViewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewReceipts") as! UITableViewController
        self.present(viewReceipts, animated: true, completion: nil)
        /*let viewReceipts = ViewReceiptsController(nibName: "ViewReceiptsController", bundle: nil)
         self.present(viewReceipts, animated: true, completion: nil)*/
    }

    
    @IBAction func ViewManagePayment(_ sender: Any) {
        let viewManagePayment = STPPaymentContextViewController(nibName: "STPPaymentContextViewController", bundle: nil)
        self.present(viewManagePayment, animated: true, completion: nil)
    }
    
    

    @IBAction func LogOutButton(_ sender: Any) {
        try! FIRAuth.auth()!.signOut()
        
        //sign out of facebook
        
        
        //sign out of facebook
        let mainStoryboard: UIStoryboard = UIStoryboard(name:"Main", bundle:nil)
        let LoginViewController: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginView")
        //let appDelegates = UIApplication.shared.delegate as! AppDelegate
        //appDelegates.window?.rootViewController = LoginViewController
        self.present(LoginViewController, animated: true, completion: nil)
    }
    //MARK: Actions
    /*@IBAction func LogOutButton(_ sender: Any) {
        
        //sign user out of Firebase app
        try! FIRAuth.auth()!.signOut()
        
        //sign out of facebook
        
        
        //sign out of facebook
        let mainStoryboard: UIStoryboard = UIStoryboard(name:"Main", bundle:nil)
        let LoginViewController: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginView")
        //let appDelegates = UIApplication.shared.delegate as! AppDelegate
        //appDelegates.window?.rootViewController = LoginViewController
        self.present(LoginViewController, animated: true, completion: nil)

        
    }
    
    
    @IBAction func ViewReceipts(_ sender: Any) {
        
            let mainStoryboard: UIStoryboard = UIStoryboard(name:"Main", bundle:nil)
            let viewReceipts: UITableViewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewReceipts") as! UITableViewController
            self.present(viewReceipts, animated: true, completion: nil)
            /*let viewReceipts = ViewReceiptsController(nibName: "ViewReceiptsController", bundle: nil)
             self.present(viewReceipts, animated: true, completion: nil)*/
        

    }
    
    
    
    @IBAction func ManagePaymentAction(_ sender: Any) {
        
        let viewManagePayment = STPPaymentContextViewController(nibName: "STPPaymentContextViewController", bundle: nil)
        self.present(viewManagePayment, animated: true, completion: nil)
    }
    */

   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //implement stripe text field delegate
        //paymentTextField.frame = CGRect(x: 15, y: 199, width: self.view.frame.width - 30, height: 44)
        //paymentTextField.delegate = self
        //view.addSubview(paymentTextField)
        //self.submitPaymentOutlet.isHidden = true
        
        //let img = createQRFromString(string: "Hello world program created by someone")
        //let somImage = UIImage(ciImage: img!, scale: 1.0, orientation: UIImageOrientation.down)
        
        //self.qrcode.image = img
        
       
        if let user = FIRAuth.auth()?.currentUser
        {
            //User is signed in
            let name = user.displayName
            
            self.fullName.text = name
        }
        else
        {
            //No user is signed in
        }
        
        //prepareView()
        // Do any additional setup after loading the view.
        
        profileImage.layer.cornerRadius = profileImage.frame.size.width/2
        profileImage.clipsToBounds = true
        
        //ProfileNavigationBar.translatesAutoresizingMaskIntoConstraints = false
        //rofileImage.translatesAutoresizingMaskIntoConstraints = false
        fullName.translatesAutoresizingMaskIntoConstraints = false
        viewReceiptsButton.translatesAutoresizingMaskIntoConstraints = false
        //viewReceiptsButton.frame.height = 50
        viewManagePaymentButton.translatesAutoresizingMaskIntoConstraints = false
        //viewManagePaymentButton.frame.height = 50
        LogOut.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(viewReceiptsButton)
        self.view.addSubview(viewManagePaymentButton)
        self.view.addSubview(fullName)
        self.view.addSubview(LogOut)
        
        
        //self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[fullName]-10-|", options: [], metrics: nil, views: ["fullName": fullName]))
        //self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[ProfileNavigationBar]-1-[fullName]|", options: [], metrics: nil, views: ["fullName": fullName, "ProfileNavigationBar": ProfileNavigationBar]))
        

        //self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[fullName]-5-[profileImage]|", options: [], metrics: nil, views: ["fullName": fullName, "profileImage" : profileImage]))
        
        let views = Dictionary(dictionaryLiteral: ("viewReceiptsButton", viewReceiptsButton), ("viewManagePaymentButton", viewManagePaymentButton), ("logOut", LogOut))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[viewReceiptsButton]-0-|", options: [], metrics: nil, views: ["viewReceiptsButton": viewReceiptsButton]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[viewManagePaymentButton]-0-|", options: [], metrics: nil, views: ["viewManagePaymentButton": viewManagePaymentButton]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[logOut]-0-|", options: [], metrics: nil, views: ["logOut": LogOut]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[viewReceiptsButton]-3-[viewManagePaymentButton]-3-[logOut]-0-|", options: [], metrics: nil, views: views))
     
        
    }
    
    /*func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        if textField.isValid {
            self.submitPaymentOutlet.isHidden = false
        }
    }
*/
    
    /*func prepareView() {
               
        managePaymentBar = ManagePaymentBar()
        
        
        //managePaymentBar.managePaymentButton.addTarget(self, action: #selector(self.MPButtonTapped), for: .touchUpInside)
       
        
        managePaymentBar.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(managePaymentBar)

        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[bottomBar]-0-|", options: [], metrics: nil, views: ["bottomBar": managePaymentBar]))
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[bottomBar]-0-|", options: [], metrics: nil, views: ["bottomBar": managePaymentBar]))
        
        /*managePaymentViewController = STPPaymentContextViewController()
        managePaymentViewController.rootViewController = self as? ProfileViewController
        managePaymentViewController.transitioningDelegate = self as? UIViewControllerTransitioningDelegate
        managePaymentViewController.modalPresentationStyle = .fullScreen
        
        //for Manage Payment
        presentInteractorMP = MiniToLargeViewInteractive()
        presentInteractorMP.attachToViewController(self, withView: managePaymentBar, presentViewController: managePaymentViewController)
        dismissInteractorMP = MiniToLargeViewInteractive()
        dismissInteractorMP.attachToViewController(managePaymentViewController, withView: managePaymentViewController.view, presentViewController: nil)*/
        
       
    }
    
  /*  func MPButtonTapped() {
        disableInteractivePlayerTransitioning = true
        self.present(managePaymentViewController, animated: true) { [unowned self] in
            self.disableInteractivePlayerTransitioning = false
        }
    }*/
    
   
/*
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }*/
    
    /*
    @IBAction func submitPaymentAction(_ sender: Any) {
        let card = paymentTextField.cardParams
        STPAPIClient.shared().createToken(withCard: card, completion: {(token, error) -> Void in
            if let error = error {
                print(error)
            }
            else if let token = token {
                print(token)
                self.chargeUsingToken(tokenn: token)
            }
        })

    }
    
    func chargeUsingToken(tokenn: STPToken) {
        
        let ref = FIRDatabase.database().reference().child("stripe_customers").child(uid!).child("sources").childByAutoId().child("token").setValue(tokenn.stripeID) { (err, resp) in
            guard err == nil else {
                print("Post failed : ")
                print(err)
                return
            }
            
            print("No errors while post")
            print(resp)
            
        }
        
    }
    
    */
    
    
    /*func createQRFromString(string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        let filter = CIFilter(name: "CIQRCodeGenerator")
        
        filter?.setValue(data, forKey: "inputMessage")
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        
        let output = filter?.outputImage?.applying(transform)
        if (output != nil)
        {
            return UIImage(ciImage: output!)
        }
        return nil;
    }*/
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
/*
extension ProfileViewController: UIViewControllerTransitioningDelegate {
    
    
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animator = MiniToLargeViewAnimator()
        animator.initialY = ManagePaymentBar.managePaymentBarHeight
        animator.transitionType = .present
        return animator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animator = MiniToLargeViewAnimator()
        animator.initialY = ManagePaymentBar.managePaymentBarHeight
        animator.transitionType = .dismiss
        return animator
    }
    
    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        guard !disableInteractivePlayerTransitioning else { return nil }
        return presentInteractorMP
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        guard !disableInteractivePlayerTransitioning else { return nil }
        return dismissInteractorMP
    }
}*/
*/
}
