//
//  CartViewController.swift
//  BreezCart
//
//  Created by Sishir Mohan on 2/17/17.
//  Copyright © 2017 Sishir Mohan. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class CartViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    
    struct cartItem {
        var itemName = ""
        var itemPrice = ""
        var itemKey = ""
    }
    
    var totalPrice = Double(0.00)
    
    @IBOutlet weak var totalPriceLabel: UILabel!

    @IBOutlet weak var cartTableView: UITableView!
    
    var ref: FIRDatabaseReference? = FIRDatabase.database().reference()
    var handleAdd: FIRDatabaseHandle?
    var getCardId: FIRDatabaseHandle?
    var handleDelete: FIRDatabaseHandle?
    var myCartList: [cartItem] = []
    var uid = FIRAuth.auth()?.currentUser?.uid
    
    var card_id = ""
    
    @IBOutlet weak var paymentDoneImage: UIImageView!
    @IBAction func SubmitPayment(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeZone = TimeZone.current
        var date = NSDate()
        totalPrice = totalPrice * 100
        ref?.child("stripe_customers").child(uid!).child("charges").childByAutoId().setValue(["amount": totalPrice, "source": card_id])
        totalPriceLabel.text = "Total"
        totalPrice = Double(0.00)
        paymentDoneImage.isHidden = false
        let handleTime = ref?.child("stripe_customers").child(uid!).child("charges").observe(.childChanged, with: { (snapshot) in
            let postDict = snapshot.value as? NSDictionary
            //print("postDict is " + postDict)
            let timestamp = postDict?["created"] as? TimeInterval
            date = NSDate(timeIntervalSince1970: timestamp!)
        })
        let localDate = dateFormatter.string(from: date as Date)
        for cartitems in myCartList {
            ref?.child("stripe_customers").child(uid!).child("recepits").child(localDate).childByAutoId().setValue(["item" : cartitems.itemName, "price": cartitems.itemPrice])
        }
 
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return myCartList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = Bundle.main.loadNibNamed("CartListCell", owner: self, options: nil)?.first as! CartListCell
        
        cell.CartListItem.text = myCartList[indexPath.row].itemName
        cell.Price.text = "$" + myCartList[indexPath.row].itemPrice
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentDoneImage.isHidden = true
        //allow deleting rows in cartTableView
        cartTableView.allowsMultipleSelectionDuringEditing = true
        
        
        //
        
        ref = FIRDatabase.database().reference()
        var itemm = cartItem()
        //observe when new child is added, append to list, and reload data
        if(uid != nil)
        {
            handleAdd = ref?.child("stripe_customers").child(uid!).child("cartList").observe(.childAdded, with: { (snapshot) in
              if let item = snapshot.value as? NSDictionary
              {
                
                itemm.itemName = item["item"] as? String ?? ""
                //print("item name is", item.itemName)
                itemm.itemPrice = item["price"] as? String ?? ""
                
                self.totalPrice = Double(itemm.itemPrice)! + self.totalPrice
                self.totalPriceLabel.text = "$" + String(self.totalPrice)
                itemm.itemKey = snapshot.key
                //print("item key is", item.itemKey)
                self.myCartList.append(itemm)
                self.cartTableView.reloadData()
              }
            })
        
            //observe the credit card
            ref?.child("stripe_customers").child(uid!).child("sources").queryLimited(toFirst: 1).observeSingleEvent(of: .value, with: { (snapshot) in
                
                if let card = snapshot.value as? NSDictionary
                {
                    let keys = Array(card.allKeys)
                    let firstKey = keys[0] as? String ?? ""
                    let dict = card.object(forKey: firstKey) as? NSDictionary
                    self.card_id = dict?.value(forKey: "id") as? String ?? ""
                    print("hello")
                }
                

            
            })
        }
        //observe when new child is removed, remove from list, and reload data
        /*handleDelete = ref?.child("users").child(uid!).child("cartList").observe(.childRemoved, with:
        { (snapshot) in
            if let item = snapshot.value as? String
            {
                
                for x in self.myCartList
                {
                    if item = x
                    {
                        let i = self.myCartList.index(of: x)
                        self.myCartList.remove(at: i!)
                    }
                }
                self.cartTableView.reloadData()
            }
            
        })*/

        // handle Delete of Cart View rows
        cartTableView.allowsMultipleSelectionDuringEditing = true
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
    
        
        let cartitem = self.myCartList[indexPath.row]
        
            ref?.child("stripe_customers").child(uid!).child("cartList").child(cartitem.itemKey).removeValue(completionBlock: {(error, ref) in
            
            if error != nil {
                print("Failed to delete message:", error as Any)
                return
            }
            self.totalPrice -= Double(self.myCartList[indexPath.row].itemPrice)!
            self.totalPriceLabel.text = "$" + String(self.totalPrice)
            self.myCartList.remove(at: indexPath.row)
            self.cartTableView.deleteRows(at: [indexPath], with: .automatic)
            
            
        })

       
        
    }
    
    
    @IBAction func ClearCart(_ sender: Any) {
        ref?.child("stripe_customers").child(uid!).child("cartList").removeValue()
        self.myCartList.removeAll()
        self.cartTableView.reloadData()
        totalPriceLabel.text = "Total"
        totalPrice = Double(0.00)
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
