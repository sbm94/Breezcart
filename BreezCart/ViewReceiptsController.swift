//
//  ViewReceiptsController.swift
//  BreezCart
//
//  Created by Sishir Mohan on 9/24/17.
//  Copyright © 2017 Sishir Mohan. All rights reserved.
//

import UIKit

class ViewReceiptsController: UITableViewController {
    
    var rootViewController: ProfileViewController?

    struct receiptDateItem {
        var date = ""
    }
    
    var dateList: [receiptDateItem] = []
    
    @IBOutlet weak var viewReceiptsItem: UINavigationItem!
     
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       /* let button = UIButton()
        button.setTitle("Dismiss", for: UIControlState())
        button.addTarget(self, action: #selector(self.doneButtonTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false*/
 
        let doneButton = UIBarButtonItem(title: "X", style: .done, target: self, action: #selector(self.doneButtonTapped))
        viewReceiptsItem.leftBarButtonItem = doneButton

        //self.view.addSubview(button)
        
        
        
    }
    
    func doneButtonTapped() {
        rootViewController?.disableInteractivePlayerTransitioning = true
        self.dismiss(animated: true) { [unowned self] in
            self.rootViewController?.disableInteractivePlayerTransitioning = false
        }
    }
    
    /*override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dateList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        <#code#>
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        <#code#>
    }
    
    */

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
