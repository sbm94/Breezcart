//
//  ManagePaymentBar.swift
//  BreezCart
//
//  Created by Sishir Mohan on 9/21/17.
//  Copyright © 2017 Sishir Mohan. All rights reserved.
//

import UIKit

class ManagePaymentBar: UIView {
    
    static let managePaymentBarHeight: CGFloat = 50
    var managePaymentButton: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupSubview() {
        self.backgroundColor = UIColor.gray
        
        managePaymentButton = UIButton()
        managePaymentButton.setTitle("Manage Payment", for: UIControlState())
        managePaymentButton.backgroundColor = UIColor.gray
        managePaymentButton.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(managePaymentButton)
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[button]-|", options: [], metrics: nil, views: ["button": managePaymentButton]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[button]-|", options: [], metrics: nil, views: ["button": managePaymentButton]))
    }
}
