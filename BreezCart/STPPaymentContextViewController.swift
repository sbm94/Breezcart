//
//  STPPaymentContextViewController.swift
//  BreezCart
//
//  Created by Sishir Mohan on 9/2/17.
//  Copyright © 2017 Sishir Mohan. All rights reserved.
//

import UIKit
import Stripe
import Firebase

class STPPaymentContextViewController: UIViewController, STPPaymentCardTextFieldDelegate{

    @IBOutlet weak var ManagePaymentItem: UINavigationItem!
    
    let paymentTextField = STPPaymentCardTextField()
    var ref: FIRDatabaseReference?
    var uid = FIRAuth.auth()?.currentUser?.uid
    var rootViewController: ProfileViewController?
    
    @IBOutlet weak var AddPaymentOutlet: UIButton!
    
    @IBOutlet weak var CardTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        paymentTextField.frame = CGRect(x: 15, y: 199, width: self.view.frame.width - 30, height: 44)
        paymentTextField.delegate = self
        view.addSubview(paymentTextField)
        self.AddPaymentOutlet.isHidden = true
        self.hideKeyboardWhenTappedAround()

        // Do any additional setup after loading the view.
        let doneButton = UIBarButtonItem(title: "X", style: .done, target: self, action: #selector(self.doneButtonTapped))
        ManagePaymentItem.leftBarButtonItem = doneButton
       
    }
    
    func doneButtonTapped() {
        rootViewController?.disableInteractivePlayerTransitioning = true
        self.dismiss(animated: true) { [unowned self] in
            self.rootViewController?.disableInteractivePlayerTransitioning = false
        }
    }
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        if textField.isValid {
            self.AddPaymentOutlet.isHidden = false
        }
    }
    
    @IBAction func SubmitPaymentAction(_ sender: Any) {
        let card = paymentTextField.cardParams
        STPAPIClient.shared().createToken(withCard: card, completion: {(token, error) -> Void in
            if let error = error {
                print(error)
            }
            else if let token = token {
                print(token)
                self.chargeUsingToken(tokenn: token)
            }
        })
    }
    
    func chargeUsingToken(tokenn: STPToken) {
        
        let ref = FIRDatabase.database().reference().child("stripe_customers").child(uid!).child("sources").childByAutoId().child("token").setValue(tokenn.stripeID) { (err, resp) in
            guard err == nil else {
                print("Post failed : ")
                print(err)
                return
            }
            
            print("No errors while post")
            print(resp)
            
        }
        
    }
   


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
