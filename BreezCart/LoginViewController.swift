//
//  LoginViewController.swift
//  BreezCart
//
//  Created by Sishir Mohan on 2/18/17.
//  Copyright © 2017 Sishir Mohan. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import GoogleSignIn


class LoginViewController: UIViewController, GIDSignInUIDelegate {
    
    
    var ref: FIRDatabaseReference!
    
    var googleButton = GIDSignInButton()

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //
        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            
            if user != nil
            {
                //User is signed in
                //move the user to another screen
                //self.googleButton.isHidden = true
                let mainStoryboard: UIStoryboard = UIStoryboard(name:"Main", bundle:nil)
                let mainViewController: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "MainView")
                let appDelegates = UIApplication.shared.delegate as! AppDelegate
                appDelegates.window?.rootViewController = mainViewController
                
                /*var top = UIApplication.shared.keyWindow?.rootViewController
                var topp = self.view.window?.rootViewController
                while(topp?.presentedViewController != nil)
                {
                    topp = topp?.presentedViewController
                }
                
                topp.present(mainViewController, animated: false, completion: nil)*/
                
                
            }
            else{
                //no user is signed in
                //show the user the login button
               /* self.view!.addSubview(self.loginButton)
                self.loginButton.center = self.view.center
                self.loginButton.delegate = self
                self.loginButton.readPermissions = ["public_profile", "email"]
                self.loginButton.isHidden = false*/
                //Google Sign in
                //self.googleButton.frame = CGRect(x: 16, y: 116+66, width: self.view.frame.width - 32, height: 50)
                
                //self.view.addSubview(self.googleButton)
                
                //GIDSignIn.sharedInstance().uiDelegate = self
                
                //self.googleButton.setUpGo
                self.activityIndicator.isHidden = true
                self.googleButton.translatesAutoresizingMaskIntoConstraints = false
                self.googleButton.frame.size = CGSize(width: self.view.frame.width - 32, height: 50)
                self.view.addSubview(self.googleButton)
                self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-30-[googleButton]-30-|", options: [], metrics: nil, views: ["googleButton": self.googleButton]))
                self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[googleButton]-160-|", options: [], metrics: nil, views: ["googleButton": self.googleButton]))
                
                GIDSignIn.sharedInstance().uiDelegate = self
                self.googleButton.addTarget(self, action: #selector(self.showActivityIndicator), for: .touchUpInside)
            }
        }
        
       
        
        
        ref = FIRDatabase.database().reference()
        

        // Do any additional setup after loading the view.
    }
    
    func showActivityIndicator() {
        self.googleButton.isHidden = true
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
    
  
    
/*
    

    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Did log out")
        
    }

    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!)
    {
        
        self.loginButton.isHidden = true
        aivLoadingSpinner.startAnimating()
        if error != nil
        {
            //handle error
            print(error)
            self.loginButton.isHidden = false
            aivLoadingSpinner.stopAnimating()
            
        }
        else if(result.isCancelled)
        {
            //handle the cancel event
            self.loginButton.isHidden = false
            aivLoadingSpinner.stopAnimating()
        }
        else
        {
        
            let accessToken = FBSDKAccessToken.current()
            guard let accessTokenString = accessToken?.tokenString else {return}
            let credentials = FIRFacebookAuthProvider.credential(withAccessToken: accessTokenString)
            FIRAuth.auth()?.signIn(with: credentials, completion: { (user, error) in
                if error != nil
                {
                    print("Something went wrong with our FB user", error ?? "")
                    return
                }
                
                print("Successfully logged in with user", user ?? "")
                
            })
            
            FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email"]).start {
                (connection, result, err) in
                
                if err != nil
                {
                    print("Failed to start graph request:" , err ?? "")
                    return
                }
                
                print(result ?? "")
                
                
            }
        }

    }*/
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func setUpGoogleButton() {
        //let googleButton = GIDSignInButton()
        //googleButton.frame = CGRect(x: 16, y: 116+66, width: view.frame.width - 32, height: 50)
        
        view.addSubview(googleButton)
        
        GIDSignIn.sharedInstance().uiDelegate = self
        
    }

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
