//
//  PopUpView.swift
//  MengProject
//
//  Created by Sishir Mohan on 1/31/17.
//  Copyright © 2017 Sishir Mohan. All rights reserved.
//

import UIKit

class PopUpView: UIView {
    
    let imageMarginSpace: CGFloat = 5.0
    var futura = UIFont(name: "Futura", size: 18.0)
    var animator: UIDynamicAnimator!
    var originalCenter: CGPoint!
    var codeText: UILabel!
    var codeTitle: UILabel!
    //var code: String!
    
    
    
    override init(frame:CGRect)
    {
        
        super.init(frame: frame)
        //self.code = codee
        self.center = center
        self.backgroundColor = UIColor.white
        //self.layer.shouldRasterize = true
        animator = UIDynamicAnimator(referenceView: self)
        self.originalCenter = super.center
        codeTitle = UILabel(frame: CGRect(x: 0.0+self.imageMarginSpace, y: 150, width: self.frame.width - (2*self.imageMarginSpace), height: self.frame.height - (2 * self.imageMarginSpace)))
        codeTitle.text = "$2.99"
        codeTitle.font = self.futura
        codeTitle.textColor = UIColor.black
        codeTitle.textAlignment = .center
        codeText = UILabel(frame: CGRect(x: 0.0+self.imageMarginSpace, y: -150, width: self.frame.width - (2*self.imageMarginSpace), height: self.frame.height - (2 * self.imageMarginSpace)))
        codeText.font = self.futura
        //codeText.text = code
        codeText.textColor = UIColor.black
        codeText.textAlignment = .center
        self.applyShadow()
        let imageName = "penne-rigate"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
        imageView.frame = CGRect(x: 22, y: 119, width: 208, height: 190)
        self.addSubview(imageView)
        self.addSubview(codeText)
        self.addSubview(codeTitle)
        
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
        
    }
    
    func applyShadow()
    {
        self.layer.cornerRadius = 6.0
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize(width: 0, height: -3)
    }
    
    func swipe(answer: Bool) {
        animator.removeAllBehaviors()
        
        // If the answer is false, Move to the left
        // Else if the answer is true, move to the right
        
        let gravityX = answer ? 0.5 : -0.5
        let magnitude = answer ? 20.0 : -20.0
        let gravityBehavior:UIGravityBehavior = UIGravityBehavior(items: [self])
        gravityBehavior.gravityDirection = CGVector(dx: CGFloat(gravityX), dy: 0)
        animator.addBehavior(gravityBehavior)
        
        let pushBehavior:UIPushBehavior = UIPushBehavior(items: [self], mode: UIPushBehaviorMode.instantaneous)
        pushBehavior.magnitude = CGFloat(magnitude)
        animator.addBehavior(pushBehavior)
        
    }
    
    func returnToCenter(BackToCenter: CGPoint) {
        UIView.animate(withDuration: 0.8, delay: 0.1, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .allowUserInteraction, animations: {
            self.center = BackToCenter
        }, completion: { finished in
            print("Finished Animation")}
        )
        
    }
    
}
