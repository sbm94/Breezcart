//
//  CartListCell.swift
//  BreezCart
//
//  Created by Sishir Mohan on 3/7/17.
//  Copyright © 2017 Sishir Mohan. All rights reserved.
//

import UIKit

class CartListCell: UITableViewCell {

    @IBOutlet weak var CartListItem: UILabel!
    
    @IBOutlet weak var Price: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
