//
//  ScannerViewController.swift
//  MengProject
//
//  Created by Sishir Mohan on 1/10/17.
//  Copyright © 2017 Sishir Mohan. All rights reserved.
//

import UIKit
import AVFoundation
import FirebaseDatabase
import FirebaseAuth

class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    var animator: UIDynamicAnimator!
    
    var imageMarginSpace: CGFloat = 10.0
    
    var originalCenter: CGPoint!
    
    var ref: FIRDatabaseReference?
    var uid = FIRAuth.auth()?.currentUser?.uid
       
    var popUpArray: [PopUpView] = []
    var popppp: PopUpView!
    
    
    @IBOutlet weak var DiscardItemButton: UIButton!
   
    @IBOutlet weak var AddToCartButton: UIButton!
    
    @IBOutlet weak var discardItemConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var addToCartConstraint: NSLayoutConstraint!
    
    
    @IBAction func AddToCart(_ sender: UIButton) {
        self.determineJudgement(judgement: true)
        

        
        viewSwipedOut()
    }

    @IBAction func DiscardItem(_ sender: Any) {
        self.determineJudgement(judgement: false)
        viewSwipedOut()
    }
    
    

    
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    
    let supportedCodeTypes = [AVMetadataObjectTypeUPCECode,
                              AVMetadataObjectTypeCode39Code,
                              AVMetadataObjectTypeCode39Mod43Code,
                              AVMetadataObjectTypeCode93Code,
                              AVMetadataObjectTypeCode128Code,
                              AVMetadataObjectTypeEAN8Code,
                              AVMetadataObjectTypeEAN13Code,
                              AVMetadataObjectTypeAztecCode,
                              AVMetadataObjectTypePDF417Code,
                              AVMetadataObjectTypeQRCode]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        popppp = PopUpView(frame: CGRect(x: 0,  y: 0, width: self.view.frame.width-75, height: self.view.frame.height-150))
        originalCenter = self.view.center
        popppp.center = originalCenter
        popUpArray.append(popppp)
        animator = UIDynamicAnimator(referenceView: popUpArray[0])
        
        UIApplication.shared.isStatusBarHidden = true
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video as the media type parameter.
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        ref = FIRDatabase.database().reference()
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            
            // Set the input device on the capture session.
            captureSession?.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            view.layer.addSublayer(videoPreviewLayer!)
            
            // Start video capture.
            captureSession?.startRunning()
            
            // Move the message label and top bar to the front
            //view.bringSubview(toFront: popUp)
            
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
                qrCodeFrameView.layer.borderWidth = 2
                view.addSubview(qrCodeFrameView)
                view.bringSubview(toFront: qrCodeFrameView)
            }
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.handlePan(gesture:)))
        popUpArray[0].addGestureRecognizer(pan)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    // MARK: - AVCaptureMetadataOutputObjectsDelegate Methods
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            //messageLabel.text = "No QR/barcode is detected"
            //popUp.code.text = "No QR/barcode is detected"
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                
                
                
                
                
               addToCartConstraint.constant = 80
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
                
                view.bringSubview(toFront: AddToCartButton)
                
                discardItemConstraint.constant = -80
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
                
                view.bringSubview(toFront: DiscardItemButton)
                
                popUpArray[0].codeText.text = "Penne Rigate"
                self.view.addSubview(popUpArray[0])
                captureSession?.stopRunning()
                
                
                //popUp.code.text = metadataObj.stringValue
            }
            
            
            
            
            
            
        }
    }
    
    func handlePan(gesture: UIPanGestureRecognizer) {
        // Is this gesture state finished??
        if gesture.state == UIGestureRecognizerState.ended {
            // Determine if we need to swipe off or return to center
            //let location = gesture.location(in: self.view)
            if popUpArray[0].center.x / self.view.bounds.maxX > 0.9 {
                self.determineJudgement(judgement: true)
                
                print("Judgement has been made")
                viewSwipedOut()
                //self.view.addSubview(popUp)
                //popUpConstraint.constant = -328
                //captureSession?.startRunning()
                
            }
            else if popUpArray[0].center.x / self.view.bounds.maxX < 0.1 {
                self.determineJudgement(judgement: false)
                
                print("Judgement has been made")
                viewSwipedOut()
                
                //popUpConstraint.constant = -328
                //captureSession?.startRunning()
                
            }
            else {
                
                //returnToCenter(popUp: popppp, originalCenter: self.originalCenter)
                self.popUpArray[0].returnToCenter(BackToCenter: originalCenter)
                
            }
        }
        let translation = gesture.translation(in: self.popUpArray[0])
        self.popUpArray[0].center = CGPoint(x: self.popUpArray[0].center.x + translation.x, y: self.popUpArray[0].center.y + translation.y)
        gesture.setTranslation(CGPoint.zero, in: self.view)
    }
    
    
    func determineJudgement(judgement: Bool)
    {
        if judgement == true
        {
            if popUpArray[0].codeText.text != ""
            {
                //ref?.child("stripe_customers").child(uid!).child("cartList").childByAutoId().setValue(popUpArray[0].codeText.text)
                    ref?.child("stripe_customers").child(uid!).child("cartList").childByAutoId().setValue(["item": popUpArray[0].codeText.text, "price": "2.99"])
            }
        }
        self.popUpArray[0].swipe(answer: judgement)
        
    }
    
    func viewSwipedOut()
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2)
        {
            
            
            self.popUpArray.remove(at: 0)
            let currentPop = PopUpView(frame: CGRect(x: 0,  y: 0, width: self.view.frame.width-75, height: self.view.frame.height-150))
            
            self.popUpArray.append(currentPop)
            self.popUpArray[0].center = self.originalCenter
            self.animator = UIDynamicAnimator(referenceView: self.popUpArray[0])
            
            UIApplication.shared.isStatusBarHidden = true
            // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video as the media type parameter.
            let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
            
            
            
            
            
            do {
                // Get an instance of the AVCaptureDeviceInput class using the previous device object.
                let input = try AVCaptureDeviceInput(device: captureDevice)
                
                // Initialize the captureSession object.
                self.captureSession = AVCaptureSession()
                
                // Set the input device on the capture session.
                self.captureSession?.addInput(input)
                
                // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
                let captureMetadataOutput = AVCaptureMetadataOutput()
                self.captureSession?.addOutput(captureMetadataOutput)
                
                // Set delegate and use the default dispatch queue to execute the call back
                captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                captureMetadataOutput.metadataObjectTypes = self.supportedCodeTypes
                
                // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
                self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
                self.videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
                self.videoPreviewLayer?.frame = self.view.layer.bounds
                self.view.layer.addSublayer(self.videoPreviewLayer!)
                
                // Start video capture.
                self.captureSession?.startRunning()
                
                // Move the message label and top bar to the front
                //view.bringSubview(toFront: popUp)
                
                // Initialize QR Code Frame to highlight the QR code
                self.qrCodeFrameView = UIView()
                
                if let qrCodeFrameView = self.qrCodeFrameView {
                    qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
                    qrCodeFrameView.layer.borderWidth = 2
                    self.view.addSubview(qrCodeFrameView)
                    self.view.bringSubview(toFront: qrCodeFrameView)
                }
                
            } catch {
                // If any error occurs, simply print it out and don't continue any more.
                print(error)
                return
            }
            
            let pan = UIPanGestureRecognizer(target: self, action: #selector(self.handlePan(gesture:)))
            self.popUpArray[0].addGestureRecognizer(pan)
        }
    }
    
    
    
    
    
}


